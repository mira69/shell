#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <dirent.h>
#include <errno.h>
#include <sys/types.h>

#include "config.h"
#include "builtins.h"
#include "wrappedio.h"

int lexit(char*[]);
int echo(char*[]);
int lcd(char*[]);
int lkill(char*[]);
int lls(char*[]);
int undefined(char *[]);

builtin_pair builtins_table[]={
	{"exit",	&lexit},
	{"lecho",	&echo},
	{"lcd",		&lcd},
	{"cd",		&lcd},
	{"lkill",	&lkill},
	{"lls",		&lls},
	{NULL,NULL}
};

int
lexit(char * argv[])
{
	exit(0);
}

int 
echo( char * argv[])
{
	int i =1;
	if (argv[i]) printf("%s", argv[i++]);
	while  (argv[i])
		printf(" %s", argv[i++]);

	printf("\n");
	fflush(stdout);
	return 0;
}

int
lcd(char * argv[])
{
	char * path = argv[1];
	if (path == NULL){
		path = getenv("HOME");
		if (path == NULL){
			return BUILTIN_ERROR;
		}
	}
	if (argv[2] != NULL){
		return BUILTIN_ERROR;
	}
	if (chdir(path) == 0){
		return 0;
	}
	return BUILTIN_ERROR;
}

int
lkill(char * argv[])
{
	int sig = SIGTERM;
	pid_t pid;
	if (argv[1] == NULL){
		return BUILTIN_ERROR;
	}
	int idx = 1;
	if (argv[1][0] == '-'){
		sig = strtol(argv[1]+1, NULL, 10);
		idx++;
		if ((!sig && (argv[1][1] != '0' || argv[1][2] != 0)) ||  argv[2] == NULL){
			return BUILTIN_ERROR;
		}
	}
	pid = strtol(argv[idx], NULL, 10);
	if (!pid && (argv[idx][0] != '0' || argv[idx][1] != 0)){
		return BUILTIN_ERROR;
	}
	if (argv[idx+1] != NULL){
		return BUILTIN_ERROR;
	}
	if (kill(pid, sig) == 0){
		return 0;
	}
	return BUILTIN_ERROR;
}

int
lls(char * argv[])
{
	char path[MAX_LINE_LENGTH];
	if (argv[1] != NULL){
		strcpy(path, argv[1]);
		if (argv[2] != NULL){
			return BUILTIN_ERROR;
		}
	}
	else if (getcwd(path, MAX_LINE_LENGTH) == NULL){
		return BUILTIN_ERROR;
	}
	DIR * dir = opendir(path);
	if (dir == NULL){
		return BUILTIN_ERROR;
	}
	struct dirent * entr;
	int errsv = errno;
	while ((entr = readdir(dir)) != NULL){
		if (entr->d_name[0] == '.'){
			continue;
		}
		mwrite(1, "%s\n", entr->d_name);
	}
	if (closedir(dir) == 0 && errno == errsv){
		return 0;
	}
	return BUILTIN_ERROR;
}

int 
undefined(char * argv[])
{
	fprintf(stderr, "Command %s undefined.\n", argv[0]);
	return BUILTIN_ERROR;
}
