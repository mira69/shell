#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <ctype.h>
#include <signal.h>
#include <sys/wait.h>
#include <sys/stat.h>

#include "builtins.h"
#include "config.h"
#include "siparse.h"
#include "utils.h"
#include "wrappedio.h"

#define bool char
#define MAX_BGCHILD_COUNT MAX_LINE_LENGTH>>1

typedef struct child{
	pid_t pid;
	int status;
} child;

volatile int chldnum = 0;
volatile pid_t chldfg[MAX_LINE_LENGTH>>1];
volatile int chldendnum = 0;
volatile child chldbg[MAX_BGCHILD_COUNT];
sigset_t waitmask;
sigset_t critmask;

void
handler(int sig_nb){
	int errsv = errno;
	pid_t pid;
	int status;
	while ((pid = waitpid(-1, &status, WNOHANG)) > 0){
		bool fg = 0;
		for (int i = 0; i < chldnum; i++){
			if (chldfg[i] == pid){
				chldfg[i] = chldfg[--chldnum];
				fg = 1;
				break;
			}
		}
		if (!fg && chldendnum < MAX_BGCHILD_COUNT){
			chldbg[chldendnum].pid = pid;
			chldbg[chldendnum].status = status;
			chldendnum++;
		}
	}
	errno = errsv;
}

//return value:
// 1 if function is local - then it had already been invoked
// 0 if shoul fork and exec
static int
localfun(char * name, char ** args){
	int funcidx = -1;
	for (int i = 0; builtins_table[i].name != NULL; i++){
		if (strcmp(name, builtins_table[i].name) == 0){
			funcidx = i;
			break;
		}
	}
	if (funcidx == -1){
		return 0;
	}
	if (builtins_table[funcidx].fun(args) == BUILTIN_ERROR){
		mwrite(2, "Builtin %s error.\n", builtins_table[funcidx].name);
	}
	return 1;
}

//return value:
// 0 on success
// -1 on failure
static int
setfdredir(const int fd, const char * name, int flags, mode_t mode){
	if (close(fd) == -1){
		return -1;
	}
	int nfd = open(name, flags, mode);
	if (nfd == -1){
		if (errno&ENOENT){
			mwrite(2, "%s: no such file or directory\n", name);
		}
		else if (errno&EACCES){
			mwrite(2, "%s: permission denied\n", name);
		}
		return -1;
	}
	if (nfd != fd){
		return -1;
	}
	return 0;
}

//return value:
// 0 on success
// -1 on failure
static int
setrdrs(command * cmd){
	redirseq *rdrs = cmd->redirs;
	if (rdrs == NULL){
		return 0;
	}
	do{
		if (IS_RIN(rdrs->r->flags)){
			if (setfdredir(0, rdrs->r->filename, O_RDONLY, 0) == -1){
				return -1;
			}
		}
		else if (IS_ROUT(rdrs->r->flags)){
			if (setfdredir(1, rdrs->r->filename, O_WRONLY|O_TRUNC|O_CREAT, S_IRUSR|S_IWUSR) == -1){
				return -1;
			}
		}
		else if (IS_RAPPEND(rdrs->r->flags)){
			if (setfdredir(1, rdrs->r->filename, O_WRONLY|O_APPEND|O_CREAT, S_IRUSR|S_IWUSR) == -1){
				return -1;
			}
		}
		rdrs = rdrs->next;
	} while (rdrs != cmd->redirs);
	return 0;
}

//a small function to avoid copying code
static int
closefd(int * fd, int idx){
	if (fd[idx] == -1){
		return 0;
	}
	if (close(fd[idx]) == -1){
		mwrite(2, "close error\n");
		return -1;
	}
	fd[idx] = -1;
	return 0;
}

//runs one external command with its arguments setting the in/out redirections
static void
runexec(command * cmd, char * args[], int * fd, bool par, bool is_first, bool is_last){
	if (!is_first && dup2(fd[2*(!par)], 0) == -1){
		exit(EXEC_FAILURE);
	}
	if (!is_last && dup2(fd[(2*par)+1], 1) == -1){
		exit(EXEC_FAILURE);
	}
	for (int i = 0; i < 4; i++){
		if (closefd(fd, i) == -1){
			exit(EXEC_FAILURE);
		}
	}
	if (setrdrs(cmd) == -1){
		close(0);
		close(1);
		exit(EXEC_FAILURE);
	}

	struct sigaction act;
	act.sa_handler = SIG_DFL;
	act.sa_flags = 0;
	sigemptyset(&act.sa_mask);
	sigaction(SIGINT, &act, NULL);
	sigaction(SIGCHLD, &act, NULL);

	execvp(cmd->args->arg, args);

	mwrite(2, "%s", cmd->args->arg);
	if (errno&ENOENT){
		mwrite(2, ": no such file or directory\n");
	}
	else if (errno&EACCES){
		mwrite(2, ": permission denied\n");
	}
	else{
		mwrite(2, ": exec error\n");
	}
	exit(EXEC_FAILURE);
}

//gets command's args and runs it either as a builtin or as an external one
//returns child's pid or 0, if a builtin command was executed
static pid_t
runcmd(command * cmd, int * fd, bool par, bool is_first, bool is_last, bool bg){
	char *args[(MAX_LINE_LENGTH>>1)+2]; //doesn't seem like a good idea, but I don't have a better one at the moment
	int n = 0;
	argseq *argsq = cmd->args;
	do{
		args[n++] = argsq->arg;
		argsq= argsq->next;
	} while(argsq!=cmd->args);
	args[n] = NULL;

	if (localfun(cmd->args->arg, args)){
		return 0;
	}

	pid_t pid = fork();
	if (pid < 0){
		mwrite(2, "fork error\n");
		exit(1); //end shell on fork error
	}
	else if (!pid){
		if (bg){
			setsid();
		}
		runexec(cmd, args, fd, par, is_first, is_last);
	}
	return pid;
}

//return value:
// 1 if given pipeline is a correct one - all commands are not null
// 0 otherwise
static bool
is_corrpipeln(pipelineseq * pln){
	commandseq * cmdseq = pln->pipeline->commands;
	do{
		if (!cmdseq->com){
			return 0;
		}
		cmdseq = cmdseq->next;
	} while(cmdseq != pln->pipeline->commands);
	return 1;
}

//runs all commands in a pipeline in the foreground
static void
runcmdseq(commandseq * cmdseq, bool bg){
	commandseq * firstcmd = cmdseq;
	int fd[4] = {-1, -1, -1, -1};
	bool par = 0; //parity of command number
	sigprocmask(SIG_BLOCK, &critmask, NULL);
	do{
		if (cmdseq->next != firstcmd){ //if it's not the last command
			if (closefd(fd, 2*par) == -1){
				exit(EXEC_FAILURE);
			}
			if (closefd(fd, (2*par)+1) == -1){
				exit(EXEC_FAILURE);
			}
			if (pipe(fd+(2*par)) == -1){
				mwrite(2, "pipe error\n");
				exit(EXEC_FAILURE);
			}
		}
		pid_t pid;
		if ((pid = runcmd(cmdseq->com, fd, par, cmdseq == firstcmd, cmdseq->next == firstcmd, bg))){
			par = !par;
			if (!bg){
				chldfg[chldnum++] = pid;
			}
		}
		cmdseq = cmdseq->next;
	} while (cmdseq != firstcmd);
	for (int i = 0; i < 4; i++){
		closefd(fd, i);
	}
	if (bg){
		return;
	}
	while (chldnum > 0){
		sigsuspend(&waitmask);
	}
	sigprocmask(SIG_UNBLOCK, &critmask, NULL);
}

//return value:
// 1 if line contains ony whitespace characters (if any)
// 0 otherwise
static bool
isempty(char * line){
	for (int i = 0; line[i] != 0; i++){
		if (!isspace(line[i])){
			return 0;
		}
	}
	return 1;
}

int
main(int argc, char *argv[])
{
	bool is_intractiv = isatty(0);
	if (!is_intractiv && !(errno&EINVAL)){
		mwrite(2, "isattyt error\n");
		exit(1);
	}

	struct sigaction actint;
	actint.sa_handler = SIG_IGN;
	actint.sa_flags = 0;
	sigemptyset(&actint.sa_mask);
	sigaction(SIGINT, &actint, NULL);

	struct sigaction actchld;
	actchld.sa_handler = handler;
	actchld.sa_flags = SA_NOCLDSTOP;
	sigemptyset(&actchld.sa_mask);
	sigaction(SIGCHLD, &actchld, NULL);

	sigfillset(&waitmask);
	sigdelset(&waitmask, SIGCHLD);

	sigemptyset(&critmask);
	sigaddset(&critmask, SIGCHLD);

	pipelineseq * ln;
	command *com;

	char buf[BUFSIZE];
	int bufbeg;


	while (1){
		//file descriptors: 0 - stdin, 1 - stdout, 2 - stderr
		if (is_intractiv){
			sigprocmask(SIG_BLOCK, &critmask, NULL);
			for (int i = 0; i < chldendnum; i++){
				mwrite(1, "Background process %d terminated. ", chldbg[i].pid);
				if (WIFEXITED(chldbg[i].status)){
					mwrite(1, "(exited with status %d)\n", WEXITSTATUS(chldbg[i].status));
				}
				else if (WIFSIGNALED(chldbg[i].status)){
					mwrite(1, "(killed by signal %d)\n", WTERMSIG(chldbg[i].status));
				}
			}
			chldendnum = 0;
			sigprocmask(SIG_UNBLOCK, &critmask, NULL);
			mwrite(1, "%s", PROMPT_STR);
		}
		int len = mread(buf, &bufbeg);
		if(len <= 0){ //errors in read result in ending shell
			if (is_intractiv){
				mwrite(1, "\n");
			}
			break;
		}
		if (len > MAX_LINE_LENGTH){
			mwrite(2, "%s\n", SYNTAX_ERROR_STR); //TODO what about write errors?
			continue;
		}
		if (isempty(buf+bufbeg)){
			continue;
		}
		ln = parseline(buf+bufbeg);
		if (!ln){
			mwrite(2, "%s\n", SYNTAX_ERROR_STR);
			continue;
		}
		pipelineseq *pln = ln;
		do{
			if (!is_corrpipeln(pln)){
				mwrite(2, "%s\n", SYNTAX_ERROR_STR);
			}
			else {
				runcmdseq(pln->pipeline->commands, pln->pipeline->flags&INBACKGROUND);
			}

		pln = pln->next;
		} while (pln != ln);
		//printparsedline(ln);
	}

	return 0;

	ln = parseline("ls -las | grep k | wc ; echo abc > f1 ;  cat < f2 ; echo abc >> f3\n");
	printparsedline(ln);
	printf("\n");
	com = pickfirstcommand(ln);
	printcommand(com,1);

	ln = parseline("sleep 3 &");
	printparsedline(ln);
	printf("\n");
	
	ln = parseline("echo  & abc >> f3\n");
	printparsedline(ln);
	printf("\n");
	com = pickfirstcommand(ln);
	printcommand(com,1);
}
