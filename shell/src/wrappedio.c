#include <unistd.h>
#include <string.h>
#include <stdarg.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

#include "config.h"
#include "wrappedio.h"

int
mread(char *buf, int *beg)
{
	static int idx_beg = 0, idx_end = 0;
	static int len = 0;
	char skipping = 0;
	while (1){
		//move data to the beginning of the buffer if at least 3/4 of it had been used
		if (len >= 3*(BUFSIZE>>2) && idx_end >= 3*(BUFSIZE>>2)){
			if (skipping){
				idx_beg = idx_end = len = 0;
			}
			else{
				memmove(buf, buf+idx_beg, len-idx_beg);
				len = len-idx_beg;
				idx_end = idx_end-idx_beg;
				idx_beg = 0;
			}
		}
		//read more if nothing left to return
		if (idx_end >= len-1){
			int tmp = read(0, buf+len, BUFSIZE-len);
			if (tmp <= 0){
				if (tmp == -1 && errno&EINTR){
					continue;
				}
				return tmp;
			}
			else{
				len += tmp;
			}
		}
		//look for the first new line
		while (idx_end < len && buf[idx_end] != '\n'){
			idx_end++;
		}
		//if line length exceeds MAX_LINE_LENGTH, skip the rest of the line and return with syntax error
		if (idx_end - idx_beg >= MAX_LINE_LENGTH){
			skipping = 1;
		}
		//if new line has been found we either fully skipped the too long line or found the next command
		if (idx_end < len){
			if (skipping){
				skipping = 0;
				*beg = idx_beg;
				idx_end++;
				idx_beg = idx_end;
				return MAX_LINE_LENGTH+1;
			}
			else{
				break;
			}
		}
	}
	buf[idx_end] = 0;
	int readlen = idx_end - idx_beg;
	*beg = idx_beg;
	while (idx_beg < idx_end && buf[idx_beg] != '#'){
		idx_beg++;
	}
	if (idx_beg < idx_end && buf[idx_beg] == '#'){ //a comment has been found and will be skipped
		buf[idx_beg] = 0;
		readlen = readlen - (idx_end - idx_beg);
	}
	idx_end++;
	idx_beg = idx_end;
	if (!readlen){ //it's 0 if only '\n' has been read causing shell to end
		return 1;
	}
	return readlen;
}

//TODO for now only '%s', '%d', and '%%' are supported
int
mwrite(int fd, char *format, ...)
{
	char str[MAX_LINE_LENGTH+1];
	str[0] = 0;
	int size = 0;
	va_list argp;
	va_start(argp, format);
	//I wanted to use sprintf for string formatting, but I don't know how to pass varargs
	for (int i = 0; format[i] != 0; i++){
		if (format[i] == '%'){
			strncat(str, format+size, i-size); //TODO buffer overflow isn't chekced
			size = i+2;
			char *arg;
			int num;
			char strnum[11];
			switch(format[i+1]){
				case 's':
					arg = va_arg(argp, char*); //WARNING behaviour undefined if too few arguments are given
					strcat(str, arg);
					break;
				case 'd':
					num = va_arg(argp, int);
					sprintf(strnum, "%d", num);
					strcat(str, strnum);
					break;
				case '%':
					strcat(str, "%");
					break;
				default:
					arg = "Unsupported string formatting\n";
					write(2, arg, strlen(arg));
					return -1;
			}
			i++;
		}
	}
	strcat(str, format+size);
	va_end(argp);

	size = strlen(str);
	int written = 0;
	while (written < size){
		int len = write(fd, str + written, size - written);
		if (len < 0){
			if (errno&EINTR){
				continue;
			}
			return len;
		}
		written += len;
	}
	return written;
}
