#ifndef _WRAPPEDIO_H_
#define _WRAPPEDIO_H_

#define BUFSIZE ((MAX_LINE_LENGTH<<1)+1)

int mread(char *buf, int *beg);
int mwrite(int fd, char *format, ...);

#endif /* !_WRAPPEDIO_H_ */
